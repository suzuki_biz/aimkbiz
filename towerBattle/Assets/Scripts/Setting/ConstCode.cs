﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 共通定数(単独の場合).
/// </summary>
public class ConstCode {
    public const string VALUE_SPLIT = "_";
    public const string NEW_LINE = "\r\n";
}

