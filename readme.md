# プロフィール  
SIer10年→ゲーム業界8年→フリーランス2021年1月から開始に向けて準備中です。  
自宅で3D製作、イラスト、アプリ、ゲーム開発もしています。  
現在はウェブ業界を目指して、gcp、go言語、ruby on railsを勉強中です。  

##Twitter  
**[ツイッター](https://twitter.com/aimkbiz)**    
##HP  
**[ホームページ](http://aimkbiz.com)**    
##技術記事(Qiita)  
**[Qiita](https://qiita.com/aimkbiz)**    

以上、ご覧、頂きありがとうございました。

# ポートフォリオ一覧  
**https://bitbucket.org/1112/aimkbiz/src/master/%E3%83%9D%E3%83%BC%E3%83%88%E3%83%95%E3%82%A9%E3%83%AA%E3%82%AA%E4%B8%80%E8%A6%A7.pdf**  

## 3D箱庭ゲーム(Unity)  
**https://bitbucket.org/1112/aimkbiz/src/master/towerBattle/**  

## 妊娠やることリスト(Unity)  
**https://bitbucket.org/1112/aimkbiz/src/master/aimkBaby/**  

## 対話型bot(Rails)  
**https://bitbucket.org/1112/aimkbiz/src/master/railsApp/**  
