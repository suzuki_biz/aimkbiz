﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 多次元配列の設定値.
/// </summary>
public class ParamList {
    public static Dictionary<long, string> TodoDivList = new Dictionary<long, string>() { 
    { 1, "母購入" } ,{2,"子購入"},{3,"やること"},{4,"手続き"}};
    public static Dictionary<long, string> TodoDiv2List = new Dictionary<long, string>() { 
    { 1, "母親の購入するモノ" } ,{2,"子供の購入するモノ"},{3,"やること"},{4,"手続き"}};
    public static Dictionary<long, string> StatusList = new Dictionary<long, string>() { 
    { 1, "未着手" } ,{2,"完了"},{3,"不要"}};
    public static Dictionary<long, string> Status2List = new Dictionary<long, string>() { 
    { 1, "未" } ,{2,"完"},{3,"不"}};
}