﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 共通定数(stringで複数の場合).
/// </summary>
public class ConstList {

    /// <summary>
    /// PlayerPrefs保存名
    /// </summary>
    public class PlayerPrefsName {
        public const string DUE_DATE = "DUE_DATE";
        public const string LAST_DATE = "LAST_DATE";
        public const string MATERNITY_DATE = "MATERNITY_DATE";
        public const string OUTPUT_DATE = "OUTPUT_DATE";
        public const string SELECT_MONTH = "SELECT_MONTH";
        public const string SELECT_TODO_DIV_LIST = "SELECT_TODO_DIV_LIST";
        public const string SELECT_NEED_LIST = "SELECT_NEED_LIST";
        public const string SELECT_STATUS_LIST = "SELECT_STATUS_LIST";
        public const string ORDER_BY = "ORDER_BY";
        public const string ORDER_BY_SORT = "ORDER_BY_SORT";
    }
}

