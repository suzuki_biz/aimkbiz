﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 共通定数(複数の場合).
/// </summary>
public class EnumList {
    /// <summary>
    /// 状態.
    /// </summary>
    public enum StatusMode {
        /// <summary> 1:未着手 </summary>
        UNTOUCHED = 1,
        /// <summary> 2:完了 </summary>
        FINSH = 2,
        /// <summary> 3:不要 </summary>
        UNNECESSARY = 3,
    }
}