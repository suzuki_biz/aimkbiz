﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


public class PanalTodoController : MonoBehaviour {
    private Vector3 _listPos;
    private Transform _panelTodo;
    private Transform _panelTodoSelect;
    private Transform _panelTodoDetail;
    private Transform _panelTodoList;
    private Text _txtMonth;
    private Text _txtProgress;
    private Text _txtDetailMonth;
    private Text _txtDetailNeed;
    private Text _txtDetailTodoDiv;
    private Text _txtDetailCount;
    private Text _txtDetailPrice;
    private Text _txtDetailStatus;
    private Text _txtDetailTitle;
    private Text _txtDetailDetail;
    private Text _txtDetailMsg;

    private InputField _inputMemoValue;

    private Button _btnTodoSelect;
    private Button _btnTodoClose;
    private Button _btnSelectSort;
    private Button _btnMonthNext;
    private Button _btnMonthAll;
    private Button _btnMonthFront;
    private Button _btnDetailClose;
    private Toggle _ckbStatus1;
    private Toggle _ckbStatus2;
    private Toggle _ckbStatus3;
    private Toggle _ckbTodoDiv1;
    private Toggle _ckbTodoDiv2;
    private Toggle _ckbTodoDiv3;
    private Toggle _ckbTodoDiv4;
    private Toggle _ckbNeed1;
    private Toggle _ckbNeed2;
    private Toggle _ckbNeed3;
    private Dictionary<int, Toggle> _rbSortList = new Dictionary<int, Toggle>();
    private Toggle _rbSortAsc;
    private Toggle _rbSortDisc;
    private Vector3 LIST_POS_DEFAULT = new Vector3(0, -50, 0);//new Vector3(0, -800, 0);
    private int _listRowSize = 220; // 200;
    private int _selectMonth = 0;
    private int _allCount = 0;
    private int _digestionCount = 0;

    private Dictionary<long, UserTodo> _userTodoList = new Dictionary<long, UserTodo>();
    private Dictionary<long, DataTodo> _dataTodoList = new Dictionary<long, DataTodo>();
    private List<GameObject> _todoList = new List<GameObject>();
    
    /// <summary>
    /// 初期設定
    /// </summary>
    protected virtual void Awake() {
        _panelTodo = transform;
        _panelTodoList = _panelTodo.FindChild("ScrollView/Content");
        _panelTodoSelect = _panelTodo.FindChild("SelectWhere");
        _panelTodoDetail = _panelTodo.FindChild("TodoDetail");

        _txtMonth = _panelTodo.FindChild("TxtMonth").GetComponent<Text>();
        _txtProgress = _panelTodo.FindChild("TxtProgress").GetComponent<Text>();
        _txtDetailMonth = _panelTodoDetail.FindChild("TxtMonthValue").GetComponent<Text>();
        _txtDetailNeed = _panelTodoDetail.FindChild("TxtNeedValue").GetComponent<Text>();
        _txtDetailTodoDiv = _panelTodoDetail.FindChild("TxtTodoDivValue").GetComponent<Text>();
        _txtDetailCount = _panelTodoDetail.FindChild("TxtCountValue").GetComponent<Text>();
        _txtDetailPrice = _panelTodoDetail.FindChild("TxtPriceValue").GetComponent<Text>();
        _txtDetailStatus = _panelTodoDetail.FindChild("TxtStatusValue").GetComponent<Text>();
        _txtDetailTitle = _panelTodoDetail.FindChild("TxtTitleValue").GetComponent<Text>();
        _txtDetailDetail = _panelTodoDetail.FindChild("TxtDetailValue").GetComponent<Text>();
        _txtDetailMsg = _panelTodoDetail.FindChild("TxtMsgValue").GetComponent<Text>();
        _inputMemoValue = _panelTodoDetail.FindChild("TxtMemoValue").GetComponent<InputField>();

        _ckbStatus1 = _panelTodoSelect.FindChild("CkbStatus1").GetComponent<Toggle>();
        _ckbStatus2 = _panelTodoSelect.FindChild("CkbStatus2").GetComponent<Toggle>();
        _ckbStatus3 = _panelTodoSelect.FindChild("CkbStatus3").GetComponent<Toggle>();
        _ckbTodoDiv1 = _panelTodoSelect.FindChild("CkbTodoDiv1").GetComponent<Toggle>();
        _ckbTodoDiv2 = _panelTodoSelect.FindChild("CkbTodoDiv2").GetComponent<Toggle>();
        _ckbTodoDiv3 = _panelTodoSelect.FindChild("CkbTodoDiv3").GetComponent<Toggle>();
        _ckbTodoDiv4 = _panelTodoSelect.FindChild("CkbTodoDiv4").GetComponent<Toggle>();
        _ckbNeed1 = _panelTodoSelect.FindChild("CkbNeed1").GetComponent<Toggle>();
        _ckbNeed2 = _panelTodoSelect.FindChild("CkbNeed2").GetComponent<Toggle>();
        _ckbNeed3 = _panelTodoSelect.FindChild("CkbNeed3").GetComponent<Toggle>();

        _rbSortAsc = _panelTodoSelect.FindChild("RbSortOrder/RbSortAsc").GetComponent<Toggle>();
        _rbSortDisc = _panelTodoSelect.FindChild("RbSortOrder/RbSortDisc").GetComponent<Toggle>();
        
        for (int i = 1; i <= 3; i++) {
            _rbSortList.Add(i, _panelTodoSelect.FindChild("RbSort/RbSort" + i).GetComponent<Toggle>());
        }
        
        _btnTodoSelect = _panelTodoSelect.FindChild("BtnSelect").GetComponent<Button>();
        _btnTodoClose = _panelTodoSelect.FindChild("BtnClose").GetComponent<Button>();
        _btnSelectSort = _panelTodo.FindChild("BtnSelectSort").GetComponent<Button>();
        _btnMonthAll = _panelTodo.FindChild("BtnMonthAll").GetComponent<Button>();
        _btnMonthNext = _panelTodo.FindChild("BtnMonthNext").GetComponent<Button>();
        _btnMonthFront = _panelTodo.FindChild("BtnMonthFront").GetComponent<Button>();
        _btnDetailClose = _panelTodoDetail.FindChild("BtnClose").GetComponent<Button>();

        _btnTodoSelect.GetComponent<Button>().onClick.AddListener(() => { OnBtnSelect(); });
        _btnTodoClose.GetComponent<Button>().onClick.AddListener(() => { OnBtnClose(); });
        _btnSelectSort.GetComponent<Button>().onClick.AddListener(() => { OnBtnSelectSort(); });
        _btnMonthAll.GetComponent<Button>().onClick.AddListener(() => { OnBtnMonth(0); });
        _btnMonthNext.GetComponent<Button>().onClick.AddListener(() => { OnBtnMonth(1); });
        _btnMonthFront.GetComponent<Button>().onClick.AddListener(() => { OnBtnMonth(-1); });
        _btnDetailClose.GetComponent<Button>().onClick.AddListener(() => { OnBtnDetailClose(); });

        if (PlayerPrefs.HasKey(ConstList.PlayerPrefsName.SELECT_MONTH)) { 
            _selectMonth = PlayerPrefs.GetInt(ConstList.PlayerPrefsName.SELECT_MONTH);
        }
        if (PlayerPrefs.HasKey(ConstList.PlayerPrefsName.SELECT_TODO_DIV_LIST)) {
            List<String> todoDivList = new List<string>(PlayerPrefs.GetString(ConstList.PlayerPrefsName.SELECT_TODO_DIV_LIST).Split(','));
            _ckbTodoDiv1.isOn = todoDivList.Contains("1");
            _ckbTodoDiv2.isOn = todoDivList.Contains("2");
            _ckbTodoDiv3.isOn = todoDivList.Contains("3");
            _ckbTodoDiv4.isOn = todoDivList.Contains("4");
        }
        if (PlayerPrefs.HasKey(ConstList.PlayerPrefsName.SELECT_NEED_LIST)) {
            List<String> needList = new List<string>(PlayerPrefs.GetString(ConstList.PlayerPrefsName.SELECT_NEED_LIST).Split(','));
            _ckbNeed1.isOn = needList.Contains("3");
            _ckbNeed2.isOn = needList.Contains("2");
            _ckbNeed3.isOn = needList.Contains("1");
        }

        if (PlayerPrefs.HasKey(ConstList.PlayerPrefsName.SELECT_STATUS_LIST)) {
            List<String> statusList = new List<string>(PlayerPrefs.GetString(ConstList.PlayerPrefsName.SELECT_STATUS_LIST).Split(','));
            _ckbStatus1.isOn = statusList.Contains("1");
            _ckbStatus2.isOn = statusList.Contains("2");
            _ckbStatus3.isOn = statusList.Contains("3");
        }

        if (PlayerPrefs.HasKey(ConstList.PlayerPrefsName.ORDER_BY)) {
            int sortKey = PlayerPrefs.GetInt(ConstList.PlayerPrefsName.ORDER_BY);
            foreach (KeyValuePair<int, Toggle> rbSort in _rbSortList) {
                if (sortKey == rbSort.Key) {
                    rbSort.Value.isOn = true;
                } else {
                    rbSort.Value.isOn = false;
                }
            }
        }

        if (PlayerPrefs.HasKey(ConstList.PlayerPrefsName.ORDER_BY_SORT)) {
            _rbSortAsc.isOn = (PlayerPrefs.GetInt(ConstList.PlayerPrefsName.ORDER_BY_SORT) == 1);
            _rbSortDisc.isOn = (PlayerPrefs.GetInt(ConstList.PlayerPrefsName.ORDER_BY_SORT) == 0);
        }
    }

    void OnEnable() {
        OnBtnTodoList();
    }

    /// <summary>
    /// ソート検索
    /// </summary>
    private void OnBtnSelectSort() {
        _panelTodo.gameObject.SetActive(true);
        _panelTodoList.gameObject.SetActive(false);
        _panelTodoSelect.gameObject.SetActive(true);
    }

    /// <summary>
    /// 月検索
    /// </summary>
    /// <param name="month"></param>
    private void OnBtnMonth(int month) {
        if (0 == month) {
            _selectMonth = 0;
        } else {
            _selectMonth += month;
            if (_selectMonth < 1) {
                _selectMonth = 10;
            } else if (_selectMonth > 10) {
                _selectMonth = 1;
            }
        }
        SetTodoList();
    }

    /// <summary>
    /// 検索
    /// </summary>
    private void OnBtnSelect() {
        SetTodoList();
    }

    /// <summary>
    /// 検索画面のクローズ
    /// </summary>
    private void OnBtnClose() {
        _panelTodoList.gameObject.SetActive(true);
        _panelTodo.gameObject.SetActive(true);
        _panelTodoSelect.gameObject.SetActive(false);
    }

    /// <summary>
    /// TODOリスト設定
    /// </summary>
    private void OnBtnTodoList() {
        SetTodoList();
    }

    /// <summary>
    /// TODOリスト設定
    /// </summary>
    private void SetTodoList(){
        _panelTodoList.gameObject.SetActive(true);
        _panelTodo.gameObject.SetActive(true);
        _panelTodoSelect.gameObject.SetActive(false);

        // 初期化
        _listPos = LIST_POS_DEFAULT;
        foreach (GameObject todo in _todoList) {
            GameObject.Destroy(todo);
        }

        _allCount = 0;
        _digestionCount = 0;

        if (!PlayerPrefs.HasKey(ConstList.PlayerPrefsName.LAST_DATE)
            || !PlayerPrefs.HasKey(ConstList.PlayerPrefsName.DUE_DATE)
            || !PlayerPrefs.HasKey(ConstList.PlayerPrefsName.MATERNITY_DATE)) {
            //OnBtnSet();
            //return;
        }

        // 月数を取得
        DateTime dtLast = DateTime.Parse(PlayerPrefs.GetString(ConstList.PlayerPrefsName.LAST_DATE));
        TimeSpan diffLast = DateTime.Now - dtLast;

        // 条件取得
        List<int> statusList = new List<int>();
        List<int> todoDivList = new List<int>();
        List<int> needList = new List<int>();

        if (_ckbStatus1.isOn) { statusList.Add(1); }
        if (_ckbStatus2.isOn) { statusList.Add(2); }
        if (_ckbStatus3.isOn) { statusList.Add(3); }

        if (_ckbTodoDiv1.isOn) { todoDivList.Add(1); }
        if (_ckbTodoDiv2.isOn) { todoDivList.Add(2); }
        if (_ckbTodoDiv3.isOn) { todoDivList.Add(3); }
        if (_ckbTodoDiv4.isOn) { todoDivList.Add(4); }

        if (_ckbNeed1.isOn) { needList.Add(3); }
        if (_ckbNeed2.isOn) { needList.Add(2); }
        if (_ckbNeed3.isOn) { needList.Add(1); }

        PlayerPrefs.SetInt(ConstList.PlayerPrefsName.SELECT_MONTH, _selectMonth);
        if (todoDivList.Count > 0) { 
            string strTodoDivList = "";
            foreach (int todoDiv in todoDivList){strTodoDivList = strTodoDivList + todoDiv.ToString() + ","; }
            PlayerPrefs.SetString(ConstList.PlayerPrefsName.SELECT_TODO_DIV_LIST, strTodoDivList.Remove(strTodoDivList.Length-1));
        }

        if (needList.Count > 0) { 
            string strNeedList = "";
            foreach (int need in needList) { strNeedList = strNeedList + need.ToString() + ","; }
            PlayerPrefs.SetString(ConstList.PlayerPrefsName.SELECT_NEED_LIST, strNeedList.Remove(strNeedList.Length - 1));
        }

        if (statusList.Count > 0) { 
            string strStatusList = "";
            foreach (int status in statusList) { strStatusList = strStatusList + status.ToString() + ","; }
            PlayerPrefs.SetString(ConstList.PlayerPrefsName.SELECT_STATUS_LIST, strStatusList.Remove(strStatusList.Length - 1));
        }

        // 一覧取得
        Dictionary<long, DataTodo> dataTodoList = new DataTodoAccess().GetListByKey(_selectMonth, todoDivList, needList);
        List<DataTodo> dataTodoListSort = new List<DataTodo>();
        _dataTodoList = new DataTodoAccess().GetList();
        _userTodoList = new UserTodoAccess().GetList();

        // 並び順
        foreach(KeyValuePair<long,DataTodo> dataTodo in dataTodoList){
            dataTodoListSort.Add(dataTodo.Value);
        }

        int sortKey = 1;
        foreach (KeyValuePair<int, Toggle> rbSort in _rbSortList) {
            if (rbSort.Value.isOn) {
                sortKey = rbSort.Key;
                if (1 == rbSort.Key) {
                    if (_rbSortAsc.isOn) {
                        dataTodoListSort.Sort(DataTodo.MonthAsc);
                    } else {
                        dataTodoListSort.Sort(DataTodo.MonthDisc);
                    }
                } else if (2 == rbSort.Key) {
                    if (_rbSortAsc.isOn) {
                        dataTodoListSort.Sort(DataTodo.TodoDivAsc);
                    } else {
                        dataTodoListSort.Sort(DataTodo.TodoDivDisc);
                    }
                } else if (3 == rbSort.Key) {
                    if (_rbSortAsc.isOn) {
                        dataTodoListSort.Sort(DataTodo.NeedAsc);
                    } else {
                        dataTodoListSort.Sort(DataTodo.NeedDisc);
                    }
                }
            }
        }

        PlayerPrefs.SetInt(ConstList.PlayerPrefsName.ORDER_BY, sortKey);
        if (_rbSortAsc.isOn) {
            PlayerPrefs.SetInt(ConstList.PlayerPrefsName.ORDER_BY_SORT, 1);
        } else {
            PlayerPrefs.SetInt(ConstList.PlayerPrefsName.ORDER_BY_SORT, 0);
        }
        
        // 一覧設定
        //int allCount = 0;
        //int digestionCount = 0;
        decimal progressCount = 0;

        foreach(DataTodo dataTodo in dataTodoListSort){
            int statusId = 1;
            if (_userTodoList.ContainsKey(dataTodo.TodoId)) {
                statusId = _userTodoList[dataTodo.TodoId].Status;
            }

            if (statusList.Contains(statusId)) {
                GameObject todoObj = GameObject.Instantiate(Resources.Load("Prefabs/TodoList"), transform.position + _listPos, Quaternion.identity) as GameObject;
                todoObj.transform.SetParent(_panelTodoList);
                //todoObj.transform.SetParent(GameObject.Find("Canvas").transform);
                todoObj.transform.localScale = Vector3.one;

                _allCount++;

                if (statusId == (int)EnumList.StatusMode.FINSH || statusId == (int)EnumList.StatusMode.UNNECESSARY) {
                    _digestionCount++;
                    todoObj.transform.FindChild("BtnTodo").GetComponent<Image>().color = Color.gray;
                    todoObj.transform.FindChild("BtnFinish").GetComponent<Image>().color = Color.gray;
                } else {
                    todoObj.transform.FindChild("BtnTodo").GetComponent<Image>().color = Color.white;
                    todoObj.transform.FindChild("BtnFinish").GetComponent<Image>().color = Color.white;
                }

                todoObj.transform.FindChild("BtnTodo/TxtName").GetComponent<Text>().text
                    = ParamList.StatusList[statusId] + "/"
                    + ParamList.TodoDivList[dataTodo.TodoDiv] + "/"
                    + dataTodo.Month + "ヶ月目/"
                    + new string('★', dataTodo.Need);
                if (dataTodo.Title.Length > 15) {
                    todoObj.transform.FindChild("BtnTodo/TxtTitle").GetComponent<Text>().text = dataTodo.Title.Substring(0,15) + "…";
                } else {
                    todoObj.transform.FindChild("BtnTodo/TxtTitle").GetComponent<Text>().text = dataTodo.Title;
                }
                todoObj.transform.FindChild("BtnFinish/Text").GetComponent<Text>().text = ParamList.Status2List[statusId];
                todoObj.transform.FindChild("BtnTodo").gameObject.GetComponent<CommonGridUIController>().SetOnClickGridListener(OnTodoDetail, (int)dataTodo.TodoId);
                todoObj.transform.FindChild("BtnFinish").gameObject.GetComponent<CommonGridUIController>().SetOnClickGridListener(OnStatusChange, (int)dataTodo.TodoId);

                //_listPos.y -= _listRowSize;
                _todoList.Add(todoObj);
            }
        }

        if (0 == _selectMonth) {
            _txtMonth.text = "全月";
        } else { 
            _txtMonth.text = _selectMonth.ToString()+"ヶ月";
        }

        if (_allCount > 0 && _digestionCount > 0) {
            progressCount = Math.Round(((decimal)_digestionCount / (decimal)_allCount) * 100, 2, MidpointRounding.AwayFromZero);
        }

        //_todoListHight = _listRowSize * allCount;
        _txtProgress.text = "全件" + _allCount + "件/消化" + _digestionCount + "件/残" + (_allCount - _digestionCount) + "件/進捗率" + progressCount + "%";
    }

    /// <summary>
    /// TODO詳細
    /// </summary>
    /// <param name="todoId"></param>
    public void OnTodoDetail(int todoId) {
        _userTodoList = new UserTodoAccess().GetList();
        int status = 1;
        _panelTodoList.gameObject.SetActive(false);
        _panelTodoDetail.gameObject.SetActive(true);
        _txtDetailMsg.gameObject.SetActive(false);
        _inputMemoValue.text = "";
        UserTodo userTodo = new UserTodo(todoId, status,"");
        _panelTodoDetail.transform.FindChild("BtnMemoSave").gameObject.GetComponent<CommonGridUIController>().SetOnClickGridListener(OnTodoMemoSave, (int)todoId);

        if (_userTodoList.ContainsKey(todoId)) {
            userTodo = _userTodoList[todoId];
        }

        if (_dataTodoList.ContainsKey(todoId)) {
            DataTodo dataTodo = _dataTodoList[todoId];
            _txtDetailMonth.text = dataTodo.Month.ToString() + "ヶ月目";
            _txtDetailNeed.text = new string('★', dataTodo.Need);
            _txtDetailTodoDiv.text = ParamList.TodoDiv2List[dataTodo.TodoDiv];
            _txtDetailStatus.text = ParamList.StatusList[userTodo.Status];
            _txtDetailTitle.text = dataTodo.Title.ToString();
            _txtDetailCount.text = "0".Equals(dataTodo.Count) ? "－" : dataTodo.Count;
            _txtDetailPrice.text = "0".Equals(dataTodo.Price) ? "－" : dataTodo.Price;
            _txtDetailDetail.text = string.IsNullOrEmpty(dataTodo.Detail) ? "詳細はありません" : dataTodo.Detail;
            _inputMemoValue.text = userTodo.Memo;
        }
    }

    /// <summary>
    /// メモをセーブする
    /// </summary>
    /// <param name="todoId"></param>
    public void OnTodoMemoSave(int todoId) {
        UserTodo userTodo = new UserTodo(todoId, 1, "");
        if (_userTodoList.ContainsKey(todoId)) {
            userTodo = _userTodoList[todoId];
        }

        userTodo.Memo = _inputMemoValue.text;
        new UserTodoAccess().SetDataList(userTodo);
        _txtDetailMsg.gameObject.SetActive(true);
    }

    /// <summary>
    /// 状態変更
    /// </summary>
    /// <param name="todoId"></param>
    public void OnStatusChange(int todoId) {
        int status = 1;
        UserTodo userTodo = new UserTodo(todoId,status,"");

        if (_userTodoList.ContainsKey(todoId)) {
            userTodo = _userTodoList[todoId];
            if (userTodo.Status < 3) {
                status = userTodo.Status+1;
            }
        } else {
            status = 2;
        }

        userTodo.Status = status;
        
        new UserTodoAccess().SetDataList(userTodo);
        
        SetTodoList();
    }

    /// <summary>
    /// 詳細画面閉じる
    /// </summary>
    private void OnBtnDetailClose() {
        _panelTodoList.gameObject.SetActive(true);
        _panelTodoDetail.gameObject.SetActive(false);
    }
}
