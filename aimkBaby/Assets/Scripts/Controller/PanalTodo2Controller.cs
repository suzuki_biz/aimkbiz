﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


public class PanalTodo2Controller : MonoBehaviour {
    private Vector3 _listPos;
    private Transform _panelTodo;
    private Transform _panelTodoSelect;
    private Transform _panelTodoDetail;
    private Transform _panelTodoList;
    private Text _txtMonth;
    private Text _txtProgress;
    private Text _txtDetailStatus;
    private Text _txtDetailTitle;
    private Text _txtDetailDetail;
    private Text _txtDetailMsg;

    private InputField _inputMemoValue;

    private Button _btnDetailClose;
    private Button _btnAllClear;

    private Vector3 LIST_POS_DEFAULT = new Vector3(0, -50, 0);//new Vector3(0, -800, 0);
    private int _listRowSize = 220; // 200;
    private int _selectMonth = 0;
    private int _allCount = 0;
    private int _digestionCount = 0;

    private Dictionary<long, UserTodo2> _userTodoList = new Dictionary<long, UserTodo2>();
    private List<GameObject> _todoList = new List<GameObject>();
    
    /// <summary>
    /// 初期設定
    /// </summary>
    protected virtual void Awake() {
        _panelTodo = transform;
        _panelTodoList = _panelTodo.FindChild("ScrollView/Content");
        _panelTodoSelect = _panelTodo.FindChild("SelectWhere");
        _panelTodoDetail = _panelTodo.FindChild("TodoDetail");

        _txtMonth = _panelTodo.FindChild("TxtMonth").GetComponent<Text>();
        _txtProgress = _panelTodo.FindChild("TxtProgress").GetComponent<Text>();
        _txtDetailStatus = _panelTodoDetail.FindChild("TxtStatusValue").GetComponent<Text>();
        _txtDetailTitle = _panelTodoDetail.FindChild("TxtTitleValue").GetComponent<Text>();
        _txtDetailDetail = _panelTodoDetail.FindChild("TxtDetailValue").GetComponent<Text>();
        _txtDetailMsg = _panelTodoDetail.FindChild("TxtMsgValue").GetComponent<Text>();
        _inputMemoValue = _panelTodoDetail.FindChild("TxtMemoValue").GetComponent<InputField>();

        _btnDetailClose = _panelTodoDetail.FindChild("BtnClose").GetComponent<Button>();
        _btnAllClear = _panelTodo.FindChild("BtnAllClear").GetComponent<Button>();

        _btnAllClear.GetComponent<Button>().onClick.AddListener(() => { OnBtnAllClear(); });
        _btnDetailClose.GetComponent<Button>().onClick.AddListener(() => { OnBtnDetailClose(); });

        if (PlayerPrefs.HasKey(ConstList.PlayerPrefsName.SELECT_MONTH)) { 
            _selectMonth = PlayerPrefs.GetInt(ConstList.PlayerPrefsName.SELECT_MONTH);
        }
        
    }

    void OnEnable() {
        OnBtnTodoList();
    }

    /// <summary>
    /// TODOリスト設定
    /// </summary>
    private void OnBtnTodoList() {
        SetTodoList();
    }

    /// <summary>
    /// TODOリスト設定
    /// </summary>
    private void SetTodoList(){
        _panelTodoList.gameObject.SetActive(true);
        _panelTodo.gameObject.SetActive(true);
        _panelTodoSelect.gameObject.SetActive(false);

        // 初期化
        _listPos = LIST_POS_DEFAULT;
        foreach (GameObject todo in _todoList) {
            GameObject.Destroy(todo);
        }

        _allCount = 0;
        _digestionCount = 0;

        // 月数を取得
        DateTime dtLast = DateTime.Parse(PlayerPrefs.GetString(ConstList.PlayerPrefsName.LAST_DATE));
        TimeSpan diffLast = DateTime.Now - dtLast;

        // 一覧取得
        _userTodoList = new UserTodo2Access().GetList();

        // 一覧設定
        decimal progressCount = 0;

        foreach (UserTodo2 dataTodo in _userTodoList.Values) {
            int statusId = 1;
            if (_userTodoList.ContainsKey(dataTodo.TodoId)) {
                statusId = _userTodoList[dataTodo.TodoId].Status;
            }

            GameObject todoObj = GameObject.Instantiate(Resources.Load("Prefabs/TodoList2"), transform.position + _listPos, Quaternion.identity) as GameObject;
            todoObj.transform.SetParent(_panelTodoList);
            //todoObj.transform.SetParent(GameObject.Find("Canvas").transform);
            todoObj.transform.localScale = Vector3.one;

            _allCount++;

            if (statusId == (int)EnumList.StatusMode.FINSH || statusId == (int)EnumList.StatusMode.UNNECESSARY) {
                _digestionCount++;
                todoObj.transform.FindChild("BtnTodo").GetComponent<Image>().color = Color.gray;
                todoObj.transform.FindChild("BtnFinish").GetComponent<Image>().color = Color.gray;
            } else {
                todoObj.transform.FindChild("BtnTodo").GetComponent<Image>().color = Color.white;
                todoObj.transform.FindChild("BtnFinish").GetComponent<Image>().color = Color.white;
            }

            todoObj.transform.FindChild("BtnTodo/TxtName").GetComponent<Text>().text
                = "";// ParamList.StatusList[statusId];
            if (dataTodo.Title.Length > 12) {
                todoObj.transform.FindChild("BtnTodo/TxtTitle").GetComponent<Text>().text = dataTodo.Title.Substring(0,12) + "…";
            } else {
                todoObj.transform.FindChild("BtnTodo/TxtTitle").GetComponent<Text>().text = dataTodo.Title;
            }
            todoObj.transform.FindChild("BtnFinish/Text").GetComponent<Text>().text = ParamList.Status2List[statusId];
            todoObj.transform.FindChild("BtnTodo").gameObject.GetComponent<CommonGridUIController>().SetOnClickGridListener(OnTodoDetail, (int)dataTodo.TodoId);
            todoObj.transform.FindChild("BtnFinish").gameObject.GetComponent<CommonGridUIController>().SetOnClickGridListener(OnStatusChange, (int)dataTodo.TodoId);

            //_listPos.y -= _listRowSize;
            _todoList.Add(todoObj);
        }

        if (_allCount > 0 && _digestionCount > 0) {
            progressCount = Math.Round(((decimal)_digestionCount / (decimal)_allCount) * 100, 2, MidpointRounding.AwayFromZero);
        }

        //_todoListHight = _listRowSize * allCount;
        _txtProgress.text = "全件" + _allCount + "件/消化" + _digestionCount + "件/残" + (_allCount - _digestionCount) + "件/進捗率" + progressCount + "%";
    }

    /// <summary>
    /// TODO詳細
    /// </summary>
    /// <param name="todoId"></param>
    public void OnTodoDetail(int todoId) {
        int status = 1;
        _panelTodoList.gameObject.SetActive(false);
        _panelTodoDetail.gameObject.SetActive(true);
        _txtDetailMsg.gameObject.SetActive(false);
        _inputMemoValue.text = "";
        UserTodo2 userTodo = new UserTodo2(todoId, status, "", "", "");
        _panelTodoDetail.transform.FindChild("BtnMemoSave").gameObject.GetComponent<CommonGridUIController>().SetOnClickGridListener(OnTodoMemoSave, (int)todoId);

        if (_userTodoList.ContainsKey(todoId)) {
            userTodo = _userTodoList[todoId];
        }

        if (_userTodoList.ContainsKey(todoId)) {
            UserTodo2 dataTodo = _userTodoList[todoId];
            _txtDetailStatus.text = ParamList.StatusList[userTodo.Status];
            _txtDetailTitle.text = dataTodo.Title.ToString();
            _txtDetailDetail.text = string.IsNullOrEmpty(dataTodo.Detail) ? "詳細はありません" : dataTodo.Detail;
            _inputMemoValue.text = userTodo.Memo;
        }
    }

    /// <summary>
    /// メモをセーブする
    /// </summary>
    /// <param name="todoId"></param>
    public void OnTodoMemoSave(int todoId) {
        UserTodo2 userTodo = new UserTodo2(todoId, 1, "", "", "");
        if (_userTodoList.ContainsKey(todoId)) {
            userTodo = _userTodoList[todoId];
        }

        userTodo.Memo = _inputMemoValue.text;
        new UserTodo2Access().SetDataList(userTodo);
        _txtDetailMsg.gameObject.SetActive(true);
    }

    /// <summary>
    /// 状態変更
    /// </summary>
    /// <param name="todoId"></param>
    public void OnStatusChange(int todoId) {
        int status = 1;
        UserTodo2 userTodo = new UserTodo2(todoId, status, "", "", "");

        if (_userTodoList.ContainsKey(todoId)) {
            userTodo = _userTodoList[todoId];
            if (userTodo.Status < 2) {
                status = userTodo.Status+1;
            }
        } else {
            status = 2;
        }

        userTodo.Status = status;
        
        new UserTodo2Access().SetDataList(userTodo);
        
        SetTodoList();
    }

    /// <summary>
    /// 全クリア
    /// </summary>
    private void OnBtnAllClear() {
        foreach (UserTodo2 dt in _userTodoList.Values) {
            UserTodo2 userTodo = _userTodoList[dt.TodoId];
            userTodo.Status = 1;
            new UserTodo2Access().SetDataList(userTodo);
        }

        SetTodoList();
    }

    /// <summary>
    /// 詳細画面閉じる
    /// </summary>
    private void OnBtnDetailClose() {
        _panelTodoList.gameObject.SetActive(true);
        _panelTodoDetail.gameObject.SetActive(false);
    }
}
