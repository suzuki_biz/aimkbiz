﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


public class PanalDayController : MonoBehaviour {
    private Text _txtLastValue;
    private Text _txtDueValue;
    private Text _txtMaternityValue;
    private Text _txtPassageValue;
    private Text _txtBalanceValue;
    private Text _txtBalance2Value;
    private Text _txtWorkingValue;
    
    /// <summary>
    /// 初期設定
    /// </summary>
    protected virtual void Awake() {
        _txtLastValue = transform.FindChild("TxtLastValue").GetComponent<Text>();
        _txtDueValue = transform.FindChild("TxtDueValue").GetComponent<Text>();
        _txtMaternityValue = transform.FindChild("TxtMaternityValue").GetComponent<Text>();
        _txtPassageValue = transform.FindChild("TxtPassageValue").GetComponent<Text>();
        _txtBalanceValue = transform.FindChild("TxtBalanceValue").GetComponent<Text>();
        _txtBalance2Value = transform.FindChild("TxtBalance2Value").GetComponent<Text>();
        _txtWorkingValue = transform.FindChild("TxtWorkingValue").GetComponent<Text>();

    }

    void OnEnable() {
        OnBtnDay();
    }

    /// <summary>
    /// 日付表示
    /// </summary>
    private void OnBtnDay() {
        transform.gameObject.SetActive(true);

        DateTime dtDue = DateTime.Parse(PlayerPrefs.GetString(ConstList.PlayerPrefsName.DUE_DATE));
        DateTime dtLast = DateTime.Parse(PlayerPrefs.GetString(ConstList.PlayerPrefsName.LAST_DATE));
        DateTime dtMaternity = DateTime.Parse(PlayerPrefs.GetString(ConstList.PlayerPrefsName.MATERNITY_DATE));

        TimeSpan diffDue = dtDue - DateTime.Now;
        TimeSpan diffLast = DateTime.Now - dtLast;
        TimeSpan diffdtMaternity = dtMaternity - DateTime.Now;
        int diffWorking = (int)Math.Ceiling((double)diffdtMaternity.Days * 0.71);
        
        _txtLastValue.text = dtLast.ToString("yyyy/M/d");
        _txtDueValue.text = dtDue.ToString("yyyy/M/d");
        _txtMaternityValue.text = dtMaternity.ToString("yyyy/M/d");
        _txtPassageValue.text = DateUtil.GetDatePassage(diffLast.Days, 0);
        _txtBalanceValue.text = diffDue.Days < 0 ? "出産予定日を過ぎました" : DateUtil.GetDatePassage(diffDue.Days, 1);
        _txtBalance2Value.text = diffdtMaternity.Days < 0 ? "産休予定日を過ぎました" : DateUtil.GetDatePassage(diffdtMaternity.Days, 1);
        _txtWorkingValue.text = diffWorking < 0 ? "産休予定日を過ぎました" : DateUtil.GetDatePassageWorking(diffWorking);
    }
}
