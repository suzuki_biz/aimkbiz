﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


public class PanalSetController : MonoBehaviour {
    private Transform _panelSet;
    private Transform _panelSetRecom;
    private Text _txtErrMsg;

    private InputField _inputLastValue;
    private InputField _inputDueValue;
    private InputField _inputMaternityValue;
    private Button _btnSetRegist;
    private Button _btnSetAuto;
    private Button _btnSetShare;
    private Button _btnSetRecom;
    private Button _btnSetRecomRecom1;
    private Button _btnSetRecomRecom2;
    private Button _btnSetRecomRecom3;
    private Button _btnSetRecomRecom4;
    private Button _btnSetRecomClose;

    /// <summary>
    /// 初期設定
    /// </summary>
    protected virtual void Awake() {
        _panelSet = transform.FindChild("PanelSet");
        _panelSetRecom = transform.FindChild("PanalRecom");

        _txtErrMsg = transform.FindChild("TxtErrMsg").GetComponent<Text>();

        _inputLastValue = transform.FindChild("TxtLastValue").GetComponent<InputField>();
        _inputDueValue = transform.FindChild("TxtDueValue").GetComponent<InputField>();
        _inputMaternityValue = transform.FindChild("TxtMaternityValue").GetComponent<InputField>();

        _btnSetRegist = transform.FindChild("BtnRegist").GetComponent<Button>();
        _btnSetAuto = transform.FindChild("BtnAuto").GetComponent<Button>();
        _btnSetShare = transform.FindChild("BtnShare").GetComponent<Button>();
        _btnSetRecom = transform.FindChild("BtnRecom").GetComponent<Button>();
        _btnSetRecomRecom1 = _panelSetRecom.FindChild("BtnRecom1").GetComponent<Button>();
        _btnSetRecomRecom2 = _panelSetRecom.FindChild("BtnRecom2").GetComponent<Button>();
        _btnSetRecomRecom3 = _panelSetRecom.FindChild("BtnRecom3").GetComponent<Button>();
        _btnSetRecomRecom4 = _panelSetRecom.FindChild("BtnRecom4").GetComponent<Button>();
        _btnSetRecomClose = _panelSetRecom.FindChild("BtnClose").GetComponent<Button>();

        _btnSetRegist.GetComponent<Button>().onClick.AddListener(() => { SaveRegist(); });
        _btnSetAuto.GetComponent<Button>().onClick.AddListener(() => { OnSetAuto(); });
        _btnSetShare.GetComponent<Button>().onClick.AddListener(() => { OnBtnShare(); });
        _btnSetRecom.GetComponent<Button>().onClick.AddListener(() => { OnBtnRecom(); });
        _btnSetRecomRecom1.GetComponent<Button>().onClick.AddListener(() => { OnBtnRecomRecom1(); });
        _btnSetRecomRecom2.GetComponent<Button>().onClick.AddListener(() => { OnBtnRecomRecom2(); });
        _btnSetRecomRecom3.GetComponent<Button>().onClick.AddListener(() => { OnBtnRecomRecom3(); });
        _btnSetRecomRecom4.GetComponent<Button>().onClick.AddListener(() => { OnBtnRecomRecom4(); });
        _btnSetRecomClose.GetComponent<Button>().onClick.AddListener(() => { OnBtnRecomClose(); });
    }

    void OnEnable() {
        OnBtnSet();
    }

    /// <summary>
    /// 日付設定の表示
    /// </summary>
    private void OnBtnSet() {

        _txtErrMsg.text = "";

        DateTime dtLast = DateTime.Now.AddMonths(-1);
        if (PlayerPrefs.HasKey(ConstList.PlayerPrefsName.LAST_DATE)) {
            dtLast = DateTime.Parse(PlayerPrefs.GetString(ConstList.PlayerPrefsName.LAST_DATE));
        }

        DateTime dtDue = DateTime.Now.AddDays(280);
        if (PlayerPrefs.HasKey(ConstList.PlayerPrefsName.DUE_DATE)) {
            dtDue = DateTime.Parse(PlayerPrefs.GetString(ConstList.PlayerPrefsName.DUE_DATE));
        }

        DateTime dtMaternity = dtDue.AddDays(-42);
        if (PlayerPrefs.HasKey(ConstList.PlayerPrefsName.MATERNITY_DATE)) {
            dtMaternity = DateTime.Parse(PlayerPrefs.GetString(ConstList.PlayerPrefsName.MATERNITY_DATE));
        }

        _inputLastValue.text = dtLast.ToString("yyyyMMdd");
        _inputDueValue.text = dtDue.ToString("yyyyMMdd");
        _inputMaternityValue.text = dtMaternity.ToString("yyyyMMdd");
    }

    /// <summary>
    /// 自動計算
    /// </summary>
    private void OnSetAuto() {
        if (_inputLastValue.text == ""
            || !DateUtil.IsDateFormat(_inputLastValue.text)) {
            _txtErrMsg.text = "最終月経は8桁で" + DateTime.Now.ToString("yyyyMMdd") + "(yyyyMMdd)の形式で入力してください";
            _txtErrMsg.color = Color.red;
            return;
        }
        
        DateTime dtLast = DateTime.Parse(DateUtil.GetDateFormat(_inputLastValue.text));
        DateTime dtDue = dtLast.AddDays(280);
        DateTime dtMaternity = dtDue.AddDays(-42);

        _txtErrMsg.text = "正常に登録しました";
        _txtErrMsg.color = Color.blue;
        PlayerPrefs.SetString(ConstList.PlayerPrefsName.LAST_DATE, dtLast.ToString("yyyy/MM/dd"));
        PlayerPrefs.SetString(ConstList.PlayerPrefsName.DUE_DATE, dtDue.ToString("yyyy/MM/dd"));
        PlayerPrefs.SetString(ConstList.PlayerPrefsName.MATERNITY_DATE, dtMaternity.ToString("yyyy/MM/dd"));
        _inputDueValue.text = dtDue.ToString("yyyyMMdd");
        _inputMaternityValue.text = dtMaternity.ToString("yyyyMMdd");
    }

    /// <summary>
    /// 日付の登録
    /// </summary>
    private void SaveRegist() {
        if (_inputLastValue.text == ""
            || !DateUtil.IsDateFormat(_inputLastValue.text)) {
                _txtErrMsg.text = "最終月経は8桁で"+ DateTime.Now.ToString("yyyyMMdd")+"(yyyyMMdd)の形式で入力してください";
                _txtErrMsg.color = Color.red;
            return;
        }

        if (_inputDueValue.text == ""
            || !DateUtil.IsDateFormat(_inputDueValue.text)) {
            _txtErrMsg.text = "予定日は8桁で" + DateTime.Now.ToString("yyyyMMdd") + "(yyyyMMdd)の形式で入力してください";
            _txtErrMsg.color = Color.red;
            return;
        }

        if (_inputMaternityValue.text == ""
            || !DateUtil.IsDateFormat(_inputMaternityValue.text)) {
            _txtErrMsg.text = "産休予定日は8桁で" + DateTime.Now.ToString("yyyyMMdd") + "(yyyyMMdd)の形式で入力してください";
            _txtErrMsg.color = Color.red;
            return;
        }
        _txtErrMsg.text = "正常に登録しました";
        _txtErrMsg.color = Color.blue;
        PlayerPrefs.SetString(ConstList.PlayerPrefsName.LAST_DATE, DateUtil.GetDateFormat(_inputLastValue.text));
        PlayerPrefs.SetString(ConstList.PlayerPrefsName.DUE_DATE, DateUtil.GetDateFormat(_inputDueValue.text));
        PlayerPrefs.SetString(ConstList.PlayerPrefsName.MATERNITY_DATE, DateUtil.GetDateFormat(_inputMaternityValue.text));
    }

    private void OnBtnShare() {
        Application.OpenURL("http://line.naver.jp/R/msg/text/?" + WWW.EscapeURL("アイミクマタニティ 妊娠中のやることリストを管理するアプリです https://play.google.com/store/apps/details?id=jp.co.aimk.aimkBaby", System.Text.Encoding.UTF8));
        //Application.OpenURL("http://line.me/R/msg/text/?アイミクマタニティ 妊娠中のやることリストを管理するアプリです https://play.google.com/store/apps/details?id=jp.co.aimk.aimkBaby");
    }

    private void OnBtnRecom() {
        _panelSetRecom.gameObject.SetActive(true);
    }

    private void OnBtnRecomRecom1() {
        Application.OpenURL("https://play.google.com/store/apps/details?id=jp.co.aimk");
    }

    private void OnBtnRecomRecom2() {
        Application.OpenURL("https://play.google.com/store/apps/details?id=jp.co.aimk.FreeGolfCounter");
    }

    private void OnBtnRecomRecom3() {
        Application.OpenURL("https://play.google.com/store/apps/details?id=jp.co.aimk.aimkSaving");
    }

    private void OnBtnRecomRecom4() {
        Application.OpenURL("https://play.google.com/store/apps/details?id=jp.co.aimk.aimkChild");
    }

    private void OnBtnRecomClose() {
        _panelSetRecom.gameObject.SetActive(false);
    }
}
