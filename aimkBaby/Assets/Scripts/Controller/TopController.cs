﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


public class TopController : MonoBehaviour {
    private Transform _panelDay;
    private Transform _panelSet;
    private Transform _panelCount;
    private Transform _panelTodo;
    private Transform _panelTodo2;
    private Transform _panelOption;

    private Button _btnDay;
    private Button _btnTodo;
    private Button _btnTodo2;
    private Button _btnSet;
    private Button _btnCount;
    
    /// <summary>
    /// 初期設定
    /// </summary>
    protected virtual void Awake() {
        _panelDay = transform.FindChild("PanelDay");
        _panelSet = transform.FindChild("PanelSet");
        _panelTodo = transform.FindChild("PanelTodo");
        _panelTodo2 = transform.FindChild("PanelTodo2");
        _panelCount = transform.FindChild("PanelCounter");
        _panelOption = transform.FindChild("PanelOption");
        
        _btnDay = transform.FindChild("BtnDay").GetComponent<Button>();
        _btnTodo = transform.FindChild("BtnTodo").GetComponent<Button>();
        _btnTodo2 = transform.FindChild("BtnTodo2").GetComponent<Button>();
        _btnSet = transform.FindChild("BtnSet").GetComponent<Button>();
        _btnCount = transform.FindChild("BtnCount").GetComponent<Button>();

        _btnDay.GetComponent<Button>().onClick.AddListener(() => { OnBtnDay(); });
        _btnTodo.GetComponent<Button>().onClick.AddListener(() => { OnBtnTodoList(); });
        _btnTodo2.GetComponent<Button>().onClick.AddListener(() => { OnBtnTodo2List(); });
        _btnSet.GetComponent<Button>().onClick.AddListener(() => { OnBtnSet(); });
        _btnCount.GetComponent<Button>().onClick.AddListener(() => { OnBtnCount(); });
		transform.FindChild("BtnOption").GetComponent<Button>().onClick.AddListener(() => { OnButtonOption(true); });
		transform.FindChild("PanelOption/BtnOptionClose").GetComponent<Button>().onClick.AddListener(() => { OnButtonOption(false); });
		transform.FindChild("PanelOption/link1").GetComponent<Button>().onClick.AddListener(() => { Application.OpenURL("https://sites.google.com/site/aimk65/privacpolicy"); });
		transform.FindChild("PanelOption/link2").GetComponent<Button>().onClick.AddListener(() => { Application.OpenURL("http://nend.net/privacy/sdkpolicy"); });


        OnBtnTodoList();
    }

    /// <summary>
    /// 陣痛カウンター
    /// </summary>
    private void OnBtnCount() {
        _panelTodo.gameObject.SetActive(false);
        _panelTodo2.gameObject.SetActive(false);
        _panelDay.gameObject.SetActive(false);
        _panelSet.gameObject.SetActive(false);
        _panelCount.gameObject.SetActive(true);
    }

    /// <summary>
    /// TODOリスト設定
    /// </summary>
    private void OnBtnTodoList() {
        _panelTodo.gameObject.SetActive(true);
        _panelTodo2.gameObject.SetActive(false);
        _panelDay.gameObject.SetActive(false);
        _panelSet.gameObject.SetActive(false);
        _panelCount.gameObject.SetActive(false);

        if (!PlayerPrefs.HasKey(ConstList.PlayerPrefsName.LAST_DATE)
        || !PlayerPrefs.HasKey(ConstList.PlayerPrefsName.DUE_DATE)
        || !PlayerPrefs.HasKey(ConstList.PlayerPrefsName.MATERNITY_DATE)) {
            OnBtnSet();
            return;
        }
    }

    /// <summary>
    /// TODOリスト(入院時)
    /// </summary>
    private void OnBtnTodo2List() {
        _panelTodo.gameObject.SetActive(false);
        _panelTodo2.gameObject.SetActive(true);
        _panelDay.gameObject.SetActive(false);
        _panelSet.gameObject.SetActive(false);
        _panelCount.gameObject.SetActive(false);

        if (!PlayerPrefs.HasKey(ConstList.PlayerPrefsName.LAST_DATE)
        || !PlayerPrefs.HasKey(ConstList.PlayerPrefsName.DUE_DATE)
        || !PlayerPrefs.HasKey(ConstList.PlayerPrefsName.MATERNITY_DATE)) {
            OnBtnSet();
            return;
        }
    }

    /// <summary>
    /// 日付設定の表示
    /// </summary>
    private void OnBtnSet() {
        _panelTodo.gameObject.SetActive(false);
        _panelTodo2.gameObject.SetActive(false);
        _panelDay.gameObject.SetActive(false);
        _panelSet.gameObject.SetActive(true);
        _panelCount.gameObject.SetActive(false);
    }

    /// <summary>
    /// 日付表示
    /// </summary>
    private void OnBtnDay() {
        if (!PlayerPrefs.HasKey(ConstList.PlayerPrefsName.LAST_DATE)
            || !PlayerPrefs.HasKey(ConstList.PlayerPrefsName.DUE_DATE)
            || !PlayerPrefs.HasKey(ConstList.PlayerPrefsName.MATERNITY_DATE)) {
            OnBtnSet();
            return;
        }

        _panelTodo.gameObject.SetActive(false);
        _panelTodo2.gameObject.SetActive(false);
        _panelDay.gameObject.SetActive(true);
        _panelSet.gameObject.SetActive(false);
        _panelCount.gameObject.SetActive(false);
    }

	/// <summary>
	/// 設定ボタンクリック
	/// </summary>
	private void OnButtonOption(Boolean isOpen) {
		_panelOption.gameObject.SetActive(isOpen);
	}
}
