﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class PanalCounterController : MonoBehaviour {
    private Transform _panalDelete;
    private Vector3 _listPos;
    private List<GameObject> _list = new List<GameObject>();

    private Button _btnStart;
    private Button _btnEnd;
    private Button _btnDeleteYes;
    private Button _btnDeleteNo;
    private Button _btnAllDelete;

    private Text _txtNowTime;

    private Vector3 LIST_POS_DEFAULT = new Vector3(0, -50, 0);//new Vector3(0, -800, 0);

    /// <summary>
    /// 初期設定
    /// </summary>
    protected virtual void Awake() {
        _panalDelete = transform.FindChild("PanalDelete");

        _txtNowTime = transform.FindChild("TxtNowTimeValue").GetComponent<Text>();

        _btnStart = transform.FindChild("BtnStart").GetComponent<Button>();
        _btnEnd = transform.FindChild("BtnEnd").GetComponent<Button>();
        _btnAllDelete = transform.FindChild("BtnAllDelete").GetComponent<Button>();
        _btnDeleteYes = _panalDelete.FindChild("BtnYes").GetComponent<Button>();
        _btnDeleteNo = _panalDelete.FindChild("BtnNo").GetComponent<Button>();
        
        _btnStart.GetComponent<Button>().onClick.AddListener(() => { OnBtnStart(); });
        _btnEnd.GetComponent<Button>().onClick.AddListener(() => { OnBtnEnd(); });
        _btnAllDelete.GetComponent<Button>().onClick.AddListener(() => { OnAllDelete(); });
        _btnDeleteYes.GetComponent<Button>().onClick.AddListener(() => { OnDeleteYes(); });
        _btnDeleteNo.GetComponent<Button>().onClick.AddListener(() => { OnDeleteNo(); });

        SetList();
    }

    void FixedUpdate() {
        _txtNowTime.text = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
    }

    private string _startTime = "";
    /// <summary>
    /// 陣痛開始
    /// </summary>
    private void OnBtnStart() {
        _startTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        UserCount userCount = new UserCount(_startTime, "");
        new UserCountAccess().SetDataList(userCount);
        SetList();
    }

    /// <summary>
    /// 陣痛終了
    /// </summary>
    private void OnBtnEnd() {
        UserCount userCount = new UserCount(_startTime, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        new UserCountAccess().SetDataList(userCount);
        SetList();
    }

    private string _deleteIndex = "";
    /// <summary>
    /// 削除
    /// </summary>
    /// <param name="todoId"></param>
    public void OnDelete(string index) {
        _deleteIndex = index;
        isAllDelete = false;
        _panalDelete.gameObject.SetActive(true);
    }

    bool isAllDelete = false;
    /// <summary>
    /// 全削除
    /// </summary>
    public void OnAllDelete() {
        isAllDelete = true;
        _panalDelete.gameObject.SetActive(true);
    }

    /// <summary>
    /// 削除時「はい」クリック
    /// </summary>
    public void OnDeleteYes() {
        if (isAllDelete) {
            new UserCountAccess().DeleteAllDataList();
        } else {
            if (_deleteIndex != "") {
                new UserCountAccess().DeleteDataList(_deleteIndex);
                _deleteIndex = "";
            }
        }
        _panalDelete.gameObject.SetActive(false);
        SetList();
    }

    /// <summary>
    /// 削除時「いいえ」クリック
    /// </summary>
    public void OnDeleteNo() {
        _deleteIndex = "";
        _panalDelete.gameObject.SetActive(false);
        SetList();
    }

    
    /// <summary>
    /// リスト設定
    /// </summary>
    private void SetList() {
        transform.gameObject.SetActive(true);

        // 初期化
        _listPos = LIST_POS_DEFAULT;
        foreach (GameObject todo in _list) {
            GameObject.Destroy(todo);
        }

        // 一覧取得
        Dictionary<int, UserCount> dtList = new UserCountAccess().GetList();
        List<UserCount> dtSortList = new List<UserCount>();
        bool isStart = true;
        foreach (UserCount dt in dtList.Values) {
            dtSortList.Add(dt);
            if (dt.EndTime == "") {
                isStart = false;
                _startTime = dt.StartTime;
            }
        }

        if (isStart) {
            _btnStart.enabled = true;
            _btnEnd.enabled = false;
            _btnStart.GetComponent<Image>().color = new Color(0.98f, 0.57f, 0.2f, 1f);
            _btnEnd.GetComponent<Image>().color = Color.gray;
        } else {
            _btnStart.enabled = false;
            _btnEnd.enabled = true;
            _btnStart.GetComponent<Image>().color = Color.gray;
            _btnEnd.GetComponent<Image>().color = new Color(0.98f, 0.57f, 0.2f, 1f);
        }
        if (dtSortList.Count > 0) { 
            dtSortList.Sort(UserCount.StartTimeDisc);
        }
        int dtIndex = 0;
        foreach (UserCount dt in dtSortList) {
            GameObject dtObj = GameObject.Instantiate(Resources.Load("Prefabs/CounterList"), transform.position + _listPos, Quaternion.identity) as GameObject;
            dtObj.transform.SetParent(transform.FindChild("ScrollView/Content"));
            dtObj.transform.localScale = Vector3.one;

            DateTime startTime = DateUtil.GetDateFormatDateTimeYYYYMMDDHHMMSS(dt.StartTime);
            DateTime endTime = DateTime.MinValue;// DateUtil.GetDateFormatDateTimeYYYYMMDDHHMMSS(dt.EndTime);
            TimeSpan diffTime = startTime - startTime;
            if (dt.EndTime != "") {
                endTime = DateUtil.GetDateFormatDateTimeYYYYMMDDHHMMSS(dt.EndTime);
                diffTime = endTime - startTime;
            }

            DateTime startFrontTime = DateUtil.GetDateFormatDateTimeYYYYMMDDHHMMSS(dt.StartTime);

            if (dtSortList.Count > (dtIndex+1)) {
                startFrontTime = DateUtil.GetDateFormatDateTimeYYYYMMDDHHMMSS(dtSortList[dtIndex + 1].StartTime);
            }

            TimeSpan diffStartTime = startTime - startFrontTime;

            dtObj.transform.FindChild("BtnTodo/TxtDay").GetComponent<Text>().text = startTime.ToString("yyyy/MM/dd");
            dtObj.transform.FindChild("BtnTodo/TxtStartTime").GetComponent<Text>().text = startTime.ToString("HH:mm:ss");
            if (diffStartTime.Hours > 0) {
                dtObj.transform.FindChild("BtnTodo/TxtDiffTime").GetComponent<Text>().text = "1時間以上";
            } else {
                dtObj.transform.FindChild("BtnTodo/TxtDiffTime").GetComponent<Text>().text = DateUtil.GetDateFormatMMSSByString(diffStartTime);
            }

            if (diffTime.Hours > 0) {
                dtObj.transform.FindChild("BtnTodo/TxtEndTime").GetComponent<Text>().text = "1時間以上";
            } else {
                if (dt.EndTime == "") {
                    dtObj.transform.FindChild("BtnTodo/TxtEndTime").GetComponent<Text>().text = "ー";
                } else {
                    dtObj.transform.FindChild("BtnTodo/TxtEndTime").GetComponent<Text>().text = DateUtil.GetDateFormatMMSSByString(diffTime);
                }
            }
            dtObj.transform.FindChild("BtnFinish").gameObject.GetComponent<CommonGridUIController>().SetOnClickGridListenerString(OnDelete, startTime.ToString("yyyy/MM/dd HH:mm:ss"));

            dtIndex++;
            _list.Add(dtObj);
        }
    }
}
