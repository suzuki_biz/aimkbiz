﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ログインデータ
/// </summary>
public class DataLogin {
    /// <summary> ログインID </summary>
    public long LoginId { get; private set; }
    /// <summary> 分類 </summary>
    public int LoginDiv { get; private set; }
    /// <summary> ログイン日数 </summary>
    public int LoginCount { get; private set; }
    /// <summary> pt </summary>
    public int Pt { get; private set; }
    
    public DataLogin(long loginId,
        int loginDiv,
        int loginCount,
        int pt) {
        this.LoginId = loginId;
        this.LoginDiv = loginDiv;
        this.LoginCount = loginCount ;
        this.Pt = pt;
    }
}

