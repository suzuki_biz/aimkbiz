﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// カウンターデータ
/// </summary>
public class UserCount {
    /// <summary> 開始時間 </summary>
    public string StartTime;
    /// <summary> 終了時間 </summary>
    public string EndTime;
    public UserCount() { }
    public UserCount(string startTime,
        string endTime) {
        this.StartTime = startTime;
        this.EndTime = endTime;
    }

    public static int StartTimeDisc(UserCount a, UserCount b) {
        return CommonUtil.GetDescDateTime(DateUtil.GetDateFormatDateTimeYYYYMMDDHHMMSS(a.StartTime), DateUtil.GetDateFormatDateTimeYYYYMMDDHHMMSS(b.StartTime));
    }

    public static int StartTimeAsc(UserCount a, UserCount b) {
        return CommonUtil.GetAskDateTime(DateUtil.GetDateFormatDateTimeYYYYMMDDHHMMSS(a.StartTime), DateUtil.GetDateFormatDateTimeYYYYMMDDHHMMSS(b.StartTime));
    }
}

