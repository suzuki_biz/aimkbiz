﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ユーザデータ
/// </summary>
public class UserData {
    /// <summary> ユーザID </summary>
    public long UserId { get; private set; }
    /// <summary> 名前 </summary>
    public string Name { get; private set; }
    /// <summary> ポイント </summary>
    public int Point { get; private set; }
    /// <summary> 最終ログイン日 </summary>
    public string LastLoginDate { get; private set; }
    /// <summary> 累計ログイン </summary>
    public int TotalLogin { get; private set; }
    public UserData() { }
    public UserData(long userId,
        string name,
        int point,
        string lastLoginDate,
        int totalLogin) {
        this.UserId = userId;
        this.Name = name;
        this.Point = point;
        this.LastLoginDate = lastLoginDate;
        this.TotalLogin = totalLogin;
    }
}

