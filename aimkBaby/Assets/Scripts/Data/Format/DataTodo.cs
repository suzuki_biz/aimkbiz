﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Todoデータ
/// </summary>
public class DataTodo {
    /// <summary> キャラID </summary>
    public long TodoId { get; private set; }
    /// <summary> 分類 </summary>
    public int TodoDiv { get; private set; }
    /// <summary> 必要度 </summary>
    public int Need { get; private set; }
    /// <summary> 月 </summary>
    public int Month { get; private set; }
    /// <summary> 個数 </summary>
    public string Count { get; private set; }
    /// <summary> 費用 </summary>
    public string Price { get; private set; }
    /// <summary> タイトル </summary>
    public string Title { get; private set; }
    /// <summary> 詳細 </summary>
    public string Detail { get; private set; }

    public static int MonthAsc(DataTodo a, DataTodo b) {
        return CommonUtil.GetAsk(a.Month, b.Month);
    }

    public static int MonthDisc(DataTodo a, DataTodo b) {
        return CommonUtil.GetDesc(a.Month, b.Month);
    }

    public static int TodoDivAsc(DataTodo a, DataTodo b) {
        return CommonUtil.GetAsk(a.TodoDiv, b.TodoDiv);
    }

    public static int TodoDivDisc(DataTodo a, DataTodo b) {
        return CommonUtil.GetDesc(a.TodoDiv, b.TodoDiv);
    }

    public static int NeedAsc(DataTodo a, DataTodo b) {
        return CommonUtil.GetAsk(a.Need, b.Need);
    }

    public static int NeedDisc(DataTodo a, DataTodo b) {
        return CommonUtil.GetDesc(a.Need, b.Need);
    }

    public DataTodo(long todoId,
        int todoDiv,
        int need,
        int month,
        string count,
        string price,
        string title,
        string detail) {
        this.TodoId = todoId;
        this.TodoDiv = todoDiv;
        this.Need = need ;
        this.Month = month;
        this.Count = count;
        this.Price = price;
        this.Title = title;
        this.Detail = detail;
    }
}

