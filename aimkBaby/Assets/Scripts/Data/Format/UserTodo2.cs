﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Todo(入院時)データ
/// </summary>
public class UserTodo2 {
    /// <summary> TODOID </summary>
    public long TodoId { get; private set; }
    /// <summary> ステータス </summary>
    public int Status;
    /// <summary> タイトル </summary>
    public string Title;
    /// <summary> タイトル </summary>
    public string Detail;
    /// <summary> メモ </summary>
    public string Memo;
    public UserTodo2() { }
    public UserTodo2(long todoId,
        int status,
        string title,
        string detail,
        string memo) {
        this.TodoId = todoId;
        this.Status = status;
        this.Title = title;
        this.Detail = detail;
        this.Memo = memo;
    }
}

