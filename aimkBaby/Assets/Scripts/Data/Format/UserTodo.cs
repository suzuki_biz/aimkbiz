﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Todoデータ
/// </summary>
public class UserTodo {
    /// <summary> TODOID </summary>
    public long TodoId { get; private set; }
    /// <summary> ステータス </summary>
    public int Status;
    /// <summary> メモ </summary>
    public string Memo;
    public UserTodo() { }
    public UserTodo(long todoId,
        int status,
        string memo) {
        this.TodoId = todoId;
        this.Status = status;
        this.Memo = memo;
    }
}

