﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

/// <summary>
/// Todoデータ
/// </summary>
public class UserTodo2Access {
    private string _tableNm = "user_todo2";

    /// <summary>
    /// 一覧情報を取得
    /// </summary>
    public Dictionary<long, UserTodo2> GetList() {

        StringCollection csvList = DBAccess.loadUserDataList(_tableNm);
        Dictionary<long, UserTodo2> dtList = new Dictionary<long, UserTodo2>();

        foreach (string csvLine in csvList) {
            string[] command = csvLine.Trim().Split(',');
            UserTodo2 dt = new UserTodo2(
            long.Parse(command[0]),
            int.Parse(command[1]),
            command.Length >= 3 ? command[2] : "",
            command.Length >= 4 ? command[3] : "",
            command.Length >= 5 ? command[4] : "");
            dtList.Add(long.Parse(command[0]), dt);
        }

        return dtList;
    }

    /// <summary>
    /// キーから情報を取得
    /// </summary>
    /// <param name="todoId"></param>
    /// <returns></returns>
    public UserTodo2 GetDataByKey(long todoId) {
        return GetList()[todoId];
    }

    /// <summary>
    /// 情報を保存
    /// </summary>
    /// <param name="todoId"></param>
    /// <returns></returns>
    public void SetDataList(UserTodo2 insertDt) {
        Dictionary<long, UserTodo2> dtList = GetList();
        StringCollection insertSql = new StringCollection();

        foreach (KeyValuePair<long, UserTodo2> todo in dtList) {
            if (todo.Value.TodoId == insertDt.TodoId) {
                insertSql.Add(insertDt.TodoId + "," + insertDt.Status + "," + insertDt.Title + "," + insertDt.Detail + "," + insertDt.Memo);
            } else {
                insertSql.Add(todo.Value.TodoId + "," + todo.Value.Status + "," + todo.Value.Title + "," + todo.Value.Detail + "," + todo.Value.Memo);
            }
        }

        if (!dtList.ContainsKey(insertDt.TodoId)) {
            insertSql.Add(insertDt.TodoId + "," + insertDt.Status + "," + insertDt.Memo);
        }

        DBAccess.saveUserDataList(_tableNm, insertSql);
    }
}
