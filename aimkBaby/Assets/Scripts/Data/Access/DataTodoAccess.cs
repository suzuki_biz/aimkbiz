﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

/// <summary>
/// Todoデータ
/// </summary>
public class DataTodoAccess {
    private string _tableNm = "data_todo";
    private static Dictionary<long, DataTodo> _dtList = null;

    /// <summary>
    /// 一覧情報を取得
    /// </summary>
    public Dictionary<long, DataTodo> GetList() {
        if (_dtList != null) {
            return _dtList;
        }

        StringCollection csvList = DBAccess.loadDataList(_tableNm);
        _dtList = new Dictionary<long, DataTodo>();

        foreach (string csvLine in csvList) {
            string[] command = csvLine.Trim().Split(',');
            DataTodo dt = new DataTodo(
            long.Parse(command[0]),
            int.Parse(command[1]),
            int.Parse(command[2]),
            int.Parse(command[3]),
            command[4],
            command[5],
            command[6],
            command[7]);
            _dtList.Add(long.Parse(command[0]), dt);
        }

        return _dtList;
    }

    /// <summary>
    /// 条件から一覧情報を取得
    /// </summary>
    public Dictionary<long, DataTodo> GetListByKey(int month,List<int> todoDivList,List<int> needList) {
        Dictionary<long, DataTodo> outputList = new Dictionary<long, DataTodo>();
        Dictionary<long, DataTodo> dtList = _dtList;
        if (_dtList == null) {
            dtList = GetList();
        }

        foreach (KeyValuePair<long ,DataTodo> dt in dtList) {
            bool isSet = true;
            if (0 < month && month != dt.Value.Month) { isSet = false; }
            if (!todoDivList.Contains(dt.Value.TodoDiv)) { isSet = false; }
            if (!needList.Contains(dt.Value.Need)) { isSet = false; }
            if (isSet) { outputList.Add(dt.Key, dt.Value); }
        }

        return outputList;
    }

    /// <summary>
    /// キーから情報を取得
    /// </summary>
    /// <param name="todoId"></param>
    /// <returns></returns>
    public DataTodo GetDataByKey(long todoId) {
        return GetList()[todoId];
    }
}
