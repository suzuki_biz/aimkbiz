﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

/// <summary>
/// Todoデータ
/// </summary>
public class UserCountAccess {
    private string _tableNm = "user_count";

    /// <summary>
    /// 一覧情報を取得
    /// </summary>
    public Dictionary<int, UserCount> GetList() {

        StringCollection csvList = DBAccess.loadUserDataList(_tableNm);
        Dictionary<int, UserCount> dtList = new Dictionary<int, UserCount>();

        int index = 0;
        foreach (string csvLine in csvList) {
            string[] command = csvLine.Trim().Split(',');
            UserCount dt = new UserCount(
            command[0],
            command[1]);
            dtList.Add(index, dt);
            index++;
        }

        return dtList;
    }

    /// <summary>
    /// 情報を保存
    /// </summary>
    /// <param name="todoId"></param>
    /// <returns></returns>
    public void SetDataList(UserCount insertDt) {
        Dictionary<int, UserCount> dtList = GetList();
        StringCollection insertSql = new StringCollection();

        StringCollection keyList = new StringCollection();
        foreach (KeyValuePair<int, UserCount> dt in dtList) {
            if (dt.Value.StartTime == insertDt.StartTime) {
                insertSql.Add(insertDt.StartTime + "," + insertDt.EndTime);
            } else {
                insertSql.Add(dt.Value.StartTime + "," + dt.Value.EndTime);
            }
            keyList.Add(dt.Value.StartTime);
        }

        if (!keyList.Contains(insertDt.StartTime)) {
            insertSql.Add(insertDt.StartTime + "," + insertDt.EndTime);
        }

        DBAccess.saveUserDataList(_tableNm, insertSql);
    }

    /// <summary>
    /// 情報を削除
    /// </summary>
    /// <param name="todoId"></param>
    /// <returns></returns>
    public void DeleteDataList(string index) {
        Dictionary<int, UserCount> dtList = GetList();
        StringCollection insertSql = new StringCollection();

        StringCollection keyList = new StringCollection();
        foreach (KeyValuePair<int, UserCount> dt in dtList) {
            if (dt.Value.StartTime != index) {
                insertSql.Add(dt.Value.StartTime + "," + dt.Value.EndTime);
            }
        }

        DBAccess.saveUserDataList(_tableNm, insertSql);
    }

    /// <summary>
    /// 全削除する
    /// </summary>
    public void DeleteAllDataList() {
        Dictionary<int, UserCount> dtList = GetList();
        StringCollection insertSql = new StringCollection();
        DBAccess.saveUserDataList(_tableNm, insertSql);
    }
}
