﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

/// <summary>
/// ユーザデータ
/// </summary>
public class UserDataAccess {
    private string _tableNm = "user_data";

    /// <summary>
    /// 一覧情報を取得
    /// </summary>
    public Dictionary<long, UserData> GetList() {

        StringCollection csvList = DBAccess.loadUserDataList(_tableNm);
        Dictionary<long, UserData> dtList = new Dictionary<long, UserData>();

        foreach (string csvLine in csvList) {
            string[] command = csvLine.Trim().Split(',');
            UserData dt = new UserData(
            long.Parse(command[0]),
            command[1],
            int.Parse(command[2]),
            command[3],
            command.Length >= 5 ? int.Parse(command[2]) : 1);
            dtList.Add(long.Parse(command[0]), dt);
        }

        return dtList;
    }

    /// <summary>
    /// キーから情報を取得
    /// </summary>
    /// <param name="todoId"></param>
    /// <returns></returns>
    public UserData GetDataByKey(long todoId) {
        return GetList()[todoId];
    }
}
