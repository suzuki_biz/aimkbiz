﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

/// <summary>
/// Todoデータ
/// </summary>
public class UserTodoAccess {
    private string _tableNm = "user_todo";

    /// <summary>
    /// 一覧情報を取得
    /// </summary>
    public Dictionary<long, UserTodo> GetList() {

        StringCollection csvList = DBAccess.loadUserDataList(_tableNm);
        Dictionary<long, UserTodo> dtList = new Dictionary<long, UserTodo>();

        foreach (string csvLine in csvList) {
            string[] command = csvLine.Trim().Split(',');
            UserTodo dt = new UserTodo(
            long.Parse(command[0]),
            int.Parse(command[1]),
            command.Length >= 3 ? command[2] : "");
            dtList.Add(long.Parse(command[0]), dt);
        }

        return dtList;
    }

    /// <summary>
    /// キーから情報を取得
    /// </summary>
    /// <param name="todoId"></param>
    /// <returns></returns>
    public UserTodo GetDataByKey(long todoId) {
        return GetList()[todoId];
    }

    /// <summary>
    /// 情報を保存
    /// </summary>
    /// <param name="todoId"></param>
    /// <returns></returns>
    public void SetDataList(UserTodo insertDt) {
        Dictionary<long, UserTodo> dtList = GetList();
        StringCollection insertSql = new StringCollection();

        foreach (KeyValuePair<long, UserTodo> todo in dtList) {
            if (todo.Value.TodoId == insertDt.TodoId) {
                insertSql.Add(insertDt.TodoId + "," + insertDt.Status + "," + insertDt.Memo);
            } else {
                insertSql.Add(todo.Value.TodoId + "," + todo.Value.Status + "," + todo.Value.Memo);
            }
        }

        if (!dtList.ContainsKey(insertDt.TodoId)) {
            insertSql.Add(insertDt.TodoId + "," + insertDt.Status + "," + insertDt.Memo);
        }

        DBAccess.saveUserDataList(_tableNm, insertSql);
    }
}
